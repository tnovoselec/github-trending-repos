package com.tnovoselec.githubtrendingrepos.ui.repos

import com.tnovoselec.githubtrendingrepos.api.ReposClient
import com.tnovoselec.githubtrendingrepos.business.Mapper
import com.tnovoselec.githubtrendingrepos.ui.BasePresenter
import com.tnovoselec.githubtrendingrepos.ui.RepoViewModel
import io.reactivex.Scheduler

class ReposPresenter(val reposClient: ReposClient,
                     val mapper: Mapper,
                     val subscribeOnScheduler: Scheduler,
                     val observeOnScheduler: Scheduler) : BasePresenter<ReposContract.View>(), ReposContract.Presenter {


    override fun getRepos() {
        view?.showProgress()

        addToUnsubscribe(
                reposClient.getTrendingRepos()
                        .map { response -> mapper.toRepoViewModels(response) }
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .subscribe({ viewModels -> onRepos(viewModels) }, { throwable -> onError(throwable) })
        )
    }

    private fun onRepos(repoViewModels: List<RepoViewModel>) {
        view?.hideProgress()
        view?.render(repoViewModels)
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
        view?.hideProgress()
        view?.showError()
    }

    override fun onRepoClicked(repoViewModel: RepoViewModel) {
    }
}