package com.tnovoselec.githubtrendingrepos.ui

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RepoViewModel(val id: Long,
                         val name: String,
                         val stars: Int,
                         val description: String,
                         val url: String,
                         val owner: String) : Parcelable