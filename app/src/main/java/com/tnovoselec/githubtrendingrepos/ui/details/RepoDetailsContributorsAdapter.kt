package com.tnovoselec.githubtrendingrepos.ui.details

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.tnovoselec.githubtrendingrepos.R
import com.tnovoselec.githubtrendingrepos.ui.RepoContributorViewModel

class RepoDetailsContributorsAdapter(val layoutInflater: LayoutInflater) : ListAdapter<RepoContributorViewModel, RepoDetailsContributorsAdapter.ContributorViewHolder>(RepoDiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContributorViewHolder {
        val view = layoutInflater.inflate(R.layout.item_contributor, parent, false)
        return ContributorViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContributorViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    class ContributorViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        @BindView(R.id.contributor_name)
        lateinit var contributorName: TextView

        @BindView(R.id.contributor_avatar)
        lateinit var contributorAvatar: ImageView


        fun bind(repoContributorViewModel: RepoContributorViewModel) {
            ButterKnife.bind(this, itemView)
            contributorName.text = repoContributorViewModel.name
            Glide.with(itemView.context).load(repoContributorViewModel.avatarUrl).apply(RequestOptions().circleCrop()).into(contributorAvatar)

        }
    }

    class RepoDiffCallback : DiffUtil.ItemCallback<RepoContributorViewModel>() {
        override fun areItemsTheSame(oldItem: RepoContributorViewModel?, newItem: RepoContributorViewModel?): Boolean {
            return oldItem?.name == newItem?.name
        }

        override fun areContentsTheSame(oldItem: RepoContributorViewModel?, newItem: RepoContributorViewModel?): Boolean {
            return oldItem == newItem
        }
    }
}