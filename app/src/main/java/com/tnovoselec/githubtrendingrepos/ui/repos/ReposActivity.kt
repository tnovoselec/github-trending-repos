package com.tnovoselec.githubtrendingrepos.ui.repos

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.tnovoselec.githubtrendingrepos.R
import com.tnovoselec.githubtrendingrepos.ui.RepoViewModel
import com.tnovoselec.githubtrendingrepos.ui.details.RepoDetailsActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class ReposActivity : AppCompatActivity(), ReposContract.View {

    @BindView(R.id.repos_recyclerview)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.repos_swiperefreshlayout)
    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    @Inject
    lateinit var reposPresenter: ReposPresenter

    lateinit var reposAdapter: ReposAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repos)
        ButterKnife.bind(this)

        AndroidInjection.inject(this)
        reposAdapter = ReposAdapter(layoutInflater)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = reposAdapter

        toolbar.title = getString(R.string.app_name)

        swipeRefreshLayout.setOnRefreshListener { reposPresenter.getRepos() }

        reposAdapter.onRepoClicked().subscribe {
            startActivity(RepoDetailsActivity.createIntent(this, it))
        }

        reposPresenter.register(this)
        reposPresenter.getRepos()
    }


    override fun onDestroy() {
        super.onDestroy()
        reposPresenter.unregister()
    }

    override fun showProgress() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideProgress() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showError() {
        Toast.makeText(this, "Oops, something went wrong..", Toast.LENGTH_SHORT).show()
    }

    override fun render(repoViewModels: List<RepoViewModel>) {
        reposAdapter.submitList(repoViewModels)
    }
}
