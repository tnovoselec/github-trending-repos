package com.tnovoselec.githubtrendingrepos.ui

data class RepoContributorViewModel(val name: String, val avatarUrl: String)