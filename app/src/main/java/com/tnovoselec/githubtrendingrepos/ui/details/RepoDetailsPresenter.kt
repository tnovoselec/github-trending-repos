package com.tnovoselec.githubtrendingrepos.ui.details

import com.tnovoselec.githubtrendingrepos.api.ReposClient
import com.tnovoselec.githubtrendingrepos.business.Mapper
import com.tnovoselec.githubtrendingrepos.ui.BasePresenter
import com.tnovoselec.githubtrendingrepos.ui.RepoContributorViewModel
import io.reactivex.Scheduler

class RepoDetailsPresenter(val reposClient: ReposClient,
                           val mapper: Mapper,
                           val subscribeOnScheduler: Scheduler,
                           val observeOnScheduler: Scheduler) : BasePresenter<RepoDetailsContract.View>(), RepoDetailsContract.Presenter {


    override fun getContributors(repoName: String, repoOwner: String) {
        view?.showProgress()

        addToUnsubscribe(
                reposClient.getContributors(repoName, repoOwner)
                        .map { mapper.toContributorViewModels(it) }
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .subscribe({ onContributors(it) }, { onError(it) })
        )
    }

    fun onContributors(contributorViewModels: List<RepoContributorViewModel>) {
        view?.hideProgress()
        view?.render(contributorViewModels)
    }

    fun onError(throwable: Throwable) {
        throwable.printStackTrace()
        view?.hideProgress()
        view?.showError()
    }


}