package com.tnovoselec.githubtrendingrepos.ui.details

import com.tnovoselec.githubtrendingrepos.ui.BasePresenterView
import com.tnovoselec.githubtrendingrepos.ui.RepoContributorViewModel

interface RepoDetailsContract {

    interface View : BasePresenterView {

        fun showProgress()
        fun hideProgress()
        fun showError()
        fun render(repoContributorViewModels: List<RepoContributorViewModel>)
    }

    interface Presenter {
        fun getContributors(repoName: String, repoOwner:String)
    }
}