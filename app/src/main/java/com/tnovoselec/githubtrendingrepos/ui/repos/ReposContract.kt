package com.tnovoselec.githubtrendingrepos.ui.repos

import com.tnovoselec.githubtrendingrepos.ui.BasePresenterView
import com.tnovoselec.githubtrendingrepos.ui.RepoViewModel

interface ReposContract {

    interface View : BasePresenterView {

        fun showProgress()
        fun hideProgress()
        fun showError()
        fun render(repoViewModels: List<RepoViewModel>)
    }

    interface Presenter {
        fun getRepos()

        fun onRepoClicked(repoViewModel: RepoViewModel)
    }
}