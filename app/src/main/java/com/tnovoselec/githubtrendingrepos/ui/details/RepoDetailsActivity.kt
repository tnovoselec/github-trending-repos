package com.tnovoselec.githubtrendingrepos.ui.details

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.tnovoselec.githubtrendingrepos.R
import com.tnovoselec.githubtrendingrepos.ui.RepoContributorViewModel
import com.tnovoselec.githubtrendingrepos.ui.RepoViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class RepoDetailsActivity : AppCompatActivity(), RepoDetailsContract.View {

    @BindView(R.id.contributors_swiperefreshlayout)
    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    @BindView(R.id.repo_details_name)
    lateinit var repoDetailsName: TextView

    @BindView(R.id.repo_details_description)
    lateinit var repoDetailsDescription: TextView

    @BindView(R.id.repo_details_owner)
    lateinit var repoDetailsOwner: TextView

    @BindView(R.id.repo_details_stars)
    lateinit var repoDetailsStars: TextView

    @BindView(R.id.repo_details_contributors)
    lateinit var recyclerView: RecyclerView

    @Inject
    lateinit var repoDetailsPresenter: RepoDetailsPresenter

    lateinit var repoDetailsContributorsAdapter: RepoDetailsContributorsAdapter

    companion object {

        val REPO_KEY = "repo_key"
        fun createIntent(context: Context, repoViewModel: RepoViewModel): Intent {
            return Intent(context, RepoDetailsActivity::class.java).apply {
                putExtra(REPO_KEY, repoViewModel)
            }
        }
    }

    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    private val repoViewModel: RepoViewModel by lazy { intent.getParcelableExtra(REPO_KEY) as RepoViewModel }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_details)
        ButterKnife.bind(this)

        AndroidInjection.inject(this)

        toolbar.title = getString(R.string.app_name)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        repoDetailsContributorsAdapter = RepoDetailsContributorsAdapter(layoutInflater)
        recyclerView.adapter = repoDetailsContributorsAdapter

        render(repoViewModel)

        repoDetailsPresenter.register(this)
        repoDetailsPresenter.getContributors(repoViewModel.name, repoViewModel.owner)
    }

    private fun render(repoViewModel: RepoViewModel) {
        repoDetailsName.text = repoViewModel.name
        repoDetailsDescription.text = repoViewModel.description
        repoDetailsOwner.text = repoViewModel.owner
        repoDetailsStars.text = repoViewModel.stars.toString()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    @OnClick(R.id.repo_details_url)
    fun onRepoUrlCLicked() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(repoViewModel.url))
        startActivity(intent)
    }

    override fun showProgress() {
        swipeRefreshLayout.isEnabled = true
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideProgress() {
        swipeRefreshLayout.isRefreshing = false
        swipeRefreshLayout.isEnabled = false
    }

    override fun showError() {
        Toast.makeText(this, "Unable to get contributors..", Toast.LENGTH_SHORT).show()
    }

    override fun render(repoContributorViewModels: List<RepoContributorViewModel>) {
        repoDetailsContributorsAdapter.submitList(repoContributorViewModels)
    }
}