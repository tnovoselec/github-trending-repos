package com.tnovoselec.githubtrendingrepos.ui.repos

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.tnovoselec.githubtrendingrepos.R
import com.tnovoselec.githubtrendingrepos.ui.RepoViewModel
import io.reactivex.subjects.PublishSubject

class ReposAdapter(val layoutInflater: LayoutInflater) : ListAdapter<RepoViewModel, ReposAdapter.RepoViewHolder>(RepoDiffCallback()) {

    private val articleSubject: PublishSubject<RepoViewModel> = PublishSubject.create()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val view = layoutInflater.inflate(R.layout.item_repo, parent, false)
        return RepoViewHolder(view)
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    fun onRepoClicked() = articleSubject

    inner class RepoViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        @BindView(R.id.repo_name)
        lateinit var repoName: TextView

        @BindView(R.id.repo_description)
        lateinit var repoDescription: TextView

        @BindView(R.id.repo_owner)
        lateinit var repoOwner: TextView

        @BindView(R.id.repo_stars)
        lateinit var repoStars: TextView

        fun bind(repoViewModel: RepoViewModel) {
            ButterKnife.bind(this, itemView)
            repoName.text = repoViewModel.name
            repoDescription.text = repoViewModel.description
            repoOwner.text = repoViewModel.owner
            repoStars.text = repoViewModel.stars.toString()
            itemView.setOnClickListener { view -> articleSubject.onNext(repoViewModel) }
        }
    }

    class RepoDiffCallback : DiffUtil.ItemCallback<RepoViewModel>() {
        override fun areItemsTheSame(oldItem: RepoViewModel?, newItem: RepoViewModel?): Boolean {
            return oldItem?.id == newItem?.id
        }

        override fun areContentsTheSame(oldItem: RepoViewModel?, newItem: RepoViewModel?): Boolean {
            return oldItem == newItem
        }
    }
}