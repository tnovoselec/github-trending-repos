package com.tnovoselec.githubtrendingrepos.api.model

import com.google.gson.annotations.SerializedName

data class ApiRepo(val id: Long,
                   val name: String,
                   @SerializedName("stargazers_count")
                   val stars: Int,
                   val description: String,
                   @SerializedName("html_url")
                   val url: String,
                   val owner: ApiRepoOwner)