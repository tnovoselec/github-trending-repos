package com.tnovoselec.githubtrendingrepos.api.model

import com.google.gson.annotations.SerializedName

data class ApiRepoOwner(@SerializedName("login") val name: String)