package com.tnovoselec.githubtrendingrepos.api

import com.tnovoselec.githubtrendingrepos.api.model.ApiContributor
import com.tnovoselec.githubtrendingrepos.api.model.RepoListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ReposApi {

    @GET("search/repositories?sort=stars&order=desc&q=android+language:android")
    fun getTrendingRepos(): Single<RepoListResponse>

    @GET("repos/{repoOwner}/{repoName}/contributors")
    fun getContributors(@Path("repoName") repoName: String, @Path("repoOwner") repoOwner: String): Single<List<ApiContributor>>
}