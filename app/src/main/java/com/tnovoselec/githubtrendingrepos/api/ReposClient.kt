package com.tnovoselec.githubtrendingrepos.api

import javax.inject.Inject

class ReposClient @Inject constructor(private val reposApi: ReposApi) {

    fun getTrendingRepos() = reposApi.getTrendingRepos()

    fun getContributors(repoName: String, repoOwner: String) = reposApi.getContributors(repoName, repoOwner)
}