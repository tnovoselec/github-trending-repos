package com.tnovoselec.githubtrendingrepos.api.model

data class RepoListResponse(val items: List<ApiRepo>)