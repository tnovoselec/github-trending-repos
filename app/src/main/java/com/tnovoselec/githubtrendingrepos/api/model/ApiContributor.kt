package com.tnovoselec.githubtrendingrepos.api.model

import com.google.gson.annotations.SerializedName

data class ApiContributor(
        @SerializedName("login")
        val name: String,
        @SerializedName("avatar_url")
        val avatarUrl: String,
        @SerializedName("html_url")
        val profileUrl: String)