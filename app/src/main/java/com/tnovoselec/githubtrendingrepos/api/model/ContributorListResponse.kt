package com.tnovoselec.githubtrendingrepos.api.model

data class ContributorListResponse(val contributors: List<ApiContributor>)