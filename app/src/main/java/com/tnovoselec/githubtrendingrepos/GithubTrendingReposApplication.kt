package com.tnovoselec.githubtrendingrepos

import android.app.Activity
import android.app.Application
import com.facebook.stetho.Stetho
import com.tnovoselec.githubtrendingrepos.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class GithubTrendingReposApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = activityDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        DaggerApplicationComponent.builder().application(this).build().injectMembers(this)
        Stetho.initializeWithDefaults(this)
    }
}