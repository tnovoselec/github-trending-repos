package com.tnovoselec.githubtrendingrepos.business

import com.tnovoselec.githubtrendingrepos.api.model.ApiContributor
import com.tnovoselec.githubtrendingrepos.api.model.RepoListResponse
import com.tnovoselec.githubtrendingrepos.ui.RepoContributorViewModel
import com.tnovoselec.githubtrendingrepos.ui.RepoViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Mapper @Inject constructor() {

    fun toRepoViewModels(repoListResponse: RepoListResponse): List<RepoViewModel> {
        val viewmodels = mutableListOf<RepoViewModel>()

        repoListResponse.items.forEach {
            viewmodels.add(RepoViewModel(it.id, it.name, it.stars, it.description, it.url, it.owner.name))
        }

        return viewmodels
    }

    fun toContributorViewModels(apiContributors: List<ApiContributor>): List<RepoContributorViewModel> {
        val viewmodels = mutableListOf<RepoContributorViewModel>()

        apiContributors.forEach {
            viewmodels.add(RepoContributorViewModel(it.name, it.avatarUrl))
        }

        return viewmodels
    }
}