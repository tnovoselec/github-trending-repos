package com.tnovoselec.githubtrendingrepos.di

import com.tnovoselec.githubtrendingrepos.GithubTrendingReposApplication
import dagger.BindsInstance
import dagger.Component
import dagger.MembersInjector
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            ApplicationModule::class,
            NetworkModule::class,
            ThreadingModule::class,
            AndroidInjectionModule::class,
            ActivityBuilder::class
        ]
)
interface ApplicationComponent : MembersInjector<GithubTrendingReposApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(githubTrendingReposApplication: GithubTrendingReposApplication): Builder

        fun build(): ApplicationComponent
    }
}