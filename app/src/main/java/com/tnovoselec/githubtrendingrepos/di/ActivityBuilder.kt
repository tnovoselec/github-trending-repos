package com.tnovoselec.githubtrendingrepos.di

import com.tnovoselec.githubtrendingrepos.ui.details.RepoDetailsActivity
import com.tnovoselec.githubtrendingrepos.ui.repos.ReposActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun bindReposActivity() : ReposActivity

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun bindRepoDetailsActivity() : RepoDetailsActivity
}