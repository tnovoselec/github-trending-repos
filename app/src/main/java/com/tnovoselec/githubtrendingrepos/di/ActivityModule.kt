package com.tnovoselec.githubtrendingrepos.di

import com.tnovoselec.githubtrendingrepos.api.ReposClient
import com.tnovoselec.githubtrendingrepos.business.Mapper
import com.tnovoselec.githubtrendingrepos.ui.details.RepoDetailsPresenter
import com.tnovoselec.githubtrendingrepos.ui.repos.ReposPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named


@Module
class ActivityModule {

    @Provides
    fun providesReposPresenter(@Named(ThreadingModule.MAIN_SCHEDULER) mainThreadScheduler: Scheduler,
                               @Named(ThreadingModule.BACKGROUND_SCHEDULER) backgroundScheduler: Scheduler,
                               mapper: Mapper,
                               reposClient: ReposClient): ReposPresenter {
        return ReposPresenter(reposClient, mapper, backgroundScheduler, mainThreadScheduler)
    }

    @Provides
    fun providesRepoDetailsPresenter(@Named(ThreadingModule.MAIN_SCHEDULER) mainThreadScheduler: Scheduler,
                                     @Named(ThreadingModule.BACKGROUND_SCHEDULER) backgroundScheduler: Scheduler,
                                     mapper: Mapper,
                                     reposClient: ReposClient): RepoDetailsPresenter {
        return RepoDetailsPresenter(reposClient, mapper, backgroundScheduler, mainThreadScheduler)
    }

}