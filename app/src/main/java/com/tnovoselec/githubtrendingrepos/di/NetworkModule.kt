package com.tnovoselec.githubtrendingrepos.di

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.tnovoselec.githubtrendingrepos.api.ReposApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    companion object {
        private val BASE_URL = "https://api.github.com"
    }

    @Provides
    fun createReposApi(): ReposApi {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .client(createOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ReposApi::class.java)
    }

    private fun createOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.addNetworkInterceptor(StethoInterceptor())
        clientBuilder.addInterceptor(loggingInterceptor)
        return clientBuilder.build()
    }
}